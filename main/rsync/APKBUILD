# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=rsync
pkgver=3.4.0
pkgrel=0
pkgdesc="A file transfer program to keep remote files in sync"
url="https://rsync.samba.org/"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	acl-dev
	attr-dev
	lz4-dev
	perl
	popt-dev
	xxhash-dev
	zlib-dev
	zstd-dev
	"
subpackages="$pkgname-doc $pkgname-openrc rrsync::noarch"
source="https://download.samba.org/pub/rsync/rsync-$pkgver.tar.gz
	https://dev.alpinelinux.org/archive/rsync-manpages-$pkgver.tar.gz
	0001-Fix-FLAG_GOT_DIR_FLIST-collission-with-FLAG_HLINKED.patch
	0001-Fix-use-after-free-in-generator.patch
	rsyncd.initd
	rsyncd.confd
	rsyncd.conf
	rsyncd.logrotate
	"

# secfixes:
#   3.4.0-r0:
#     - CVE-2024-12084
#     - CVE-2024-12085
#     - CVE-2024-12086
#     - CVE-2024-12087
#     - CVE-2024-12088
#     - CVE-2024-12747
#   3.2.4-r2:
#     - CVE-2022-29154
#   3.1.2-r7:
#     - CVE-2017-16548
#     - CVE-2017-17433
#     - CVE-2017-17434
#   0:
#     - CVE-2020-14387

# delete the itemize test because it is flawed and depends on filesystem-specific behavior
prepare() {
	default_prepare
	update_config_sub

	rm testsuite/itemize.test

	# Prevent the aports version being used
	printf '#!/bin/sh\n\necho "#define RSYNC_GITVER RSYNC_VERSION" >git-version.h\n' >mkgitver
}

build() {
	cp  rrsync.1 support/rrsync.1
	CFLAGS="$CFLAGS -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-acl-support \
		--enable-xattr-support \
		--enable-xxhash \
		--with-rrsync \
		--without-included-popt \
		--without-included-zlib \
		--disable-md2man \
		--disable-openssl
		# openssl is disabled since xxh checksums are faster anyway
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm755 "$srcdir"/rsyncd.initd "$pkgdir"/etc/init.d/rsyncd
	install -Dm644 "$srcdir"/rsyncd.conf "$pkgdir"/etc/rsyncd.conf
	install -Dm644 "$srcdir"/rsyncd.confd "$pkgdir"/etc/conf.d/rsyncd
	install -Dm644 "$srcdir"/rsyncd.logrotate "$pkgdir"/etc/logrotate.d/rsyncd
}

rrsync() {
	pkgdesc="Restricted rsync, restricts rsync to a subdir declared in .ssh/authorized_keys"
	depends="rsync python3"

	amove usr/bin/rrsync
}

sha512sums="
4a0e7817e6e71e0173713ddd6b6bc7ee86237d092bd0a8c830a69f9993b76b5712a13a3ca60c7bbf42162cdc837df8783e07f8cd65c32fcb12c35f751043c56b  rsync-3.4.0.tar.gz
5808533ffbddfb0a927c68d6f2c548650d0ce9ed698c74906eb5f6e997a40c540831a73e2ae546b6cd216de0593dfaa75810205e00022935f269b85134425ab7  rsync-manpages-3.4.0.tar.gz
5d3fdb72df94f2512db15cb8759bc0fc6d3d7a889d6291ffef3327f715cf03f8950eaefbaa9ad528a0ddf97ab4bb011935441884a857e7dde50eb9e78287dd9d  0001-Fix-FLAG_GOT_DIR_FLIST-collission-with-FLAG_HLINKED.patch
b8464659b18af0208cc44cb11dc8d7faf8b5c504aacc56b29191c470d04e40c42c79a4d7abe2af8b31fdb644c7b76075a4195257d8c25f5ce0c05e173997467d  0001-Fix-use-after-free-in-generator.patch
b9bf1aa02f96e4294642ead5751bd529ca1267c08e83a16342fba5736c3a8ec89568feb11fb737e974cb1bee7e00e7a8898d25844892366c6167b9ea8d1e647c  rsyncd.initd
d91337cfb57e6e3b2a8ba1e24f7d851dd927bfc327da2212b9eb0acda0e1ca2f24987f6dcc4903eccc3bf170e0f115172b3cfa5a172700495296f26302c834d7  rsyncd.confd
3db8a2b364fc89132af6143af90513deb6be3a78c8180d47c969e33cb5edde9db88aad27758a6911f93781e3c9846aeadc80fffc761c355d6a28358853156b62  rsyncd.conf
e7ff164926785c4eff2ea641c7ce2d270b25f1c26d93a6108bb6ff2c0207a28ebfd93dca39596243446ce41aceaeae62fc2b34084eb9c9086fcdbc03a657eed8  rsyncd.logrotate
"
