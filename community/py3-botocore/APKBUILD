# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-botocore
# Verify required version from py3-boto3 on this package before upgrading
pkgver=1.35.92
pkgrel=0
pkgdesc="The low-level, core functionality of Boto3"
url="https://github.com/boto/botocore"
arch="noarch"
license="Apache-2.0"
depends="
	py3-certifi
	py3-dateutil
	py3-jmespath
	py3-urllib3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/b/botocore/botocore-$pkgver.tar.gz"
builddir="$srcdir/botocore-$pkgver"
options="!check" # take way too long

replaces=py-botocore # Backwards compatibility
provides=py-botocore=$pkgver-r$pkgrel # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
f4ed905ecc4545e1ebfdd9244947e29a2a9d6f1e580a6ea4e7ee0a07320cefd50308b663c6773d210a259283db5ae1073e3e0bfe2b75f9a9bb5640735c4612e4  botocore-1.35.92.tar.gz
"
